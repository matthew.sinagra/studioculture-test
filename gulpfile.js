var gulp = require('gulp');
var sass = require('gulp-sass');
var apf = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var watch = require('gulp-watch')
var connect = require('gulp-connect');
var plumber = require('gulp-plumber');
var cleanCSS = require('gulp-clean-css');
var tiny = require('gulp-tinypng-nokey');


gulp.task('styles', function() {
    gulp.src('assets/sass/*.scss')
        .pipe(sass({
            includePaths: require('node-normalize-scss').includePaths
        }))
        .pipe(sass()).on('error', function(error) {
            // we have an error
            done(error); 
          })
        .pipe(gulp.dest('assets/css'))
        .pipe(connect.reload());
});



gulp.task('tinypng', function(cb) {
    gulp.src('assets/imgs/*')
        .pipe(tiny())
        .pipe(gulp.dest('assets/imgs/*'));
});


gulp.task('serve', function() {
    connect.server({
        livereload: true,
        root: ['.', '.tmp']
      });
  });

  gulp.task('livereload', function() {
    gulp.src(['assets/css/*.css', '.assets/js/*.js'])
        .pipe(plumber())
        .pipe(watch('assets/css/*.css'))
        .pipe(watch('*.html'))
        .pipe(connect.reload());
  });

//prefix/min/everythingelse
gulp.task('preprod', function() {
    gulp.src('assets/css/styles.css')
        .pipe(plumber())
        .pipe(apf({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('style.prod.css'))
        .pipe(gulp.dest('assets/css/'))
})

gulp.task('watch', function() {
    gulp.watch('assets/**/*.scss', ['styles'])
    
})
//Watch task
//gulp.task('default', ['styles', 'serve', 'watch']);

gulp.task('default', ['styles', 'serve', 'livereload', 'watch']);